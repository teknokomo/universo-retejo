export default {
  namespaced: true,

  state() {
    return {
      show_login_form: false,
      messages: [],
      snackbar: false,
    }
  },

  getters: {

  },

  mutations: {
    setSnackbar(state, val) {
      const old_stat = state.snackbar

      state.snackbar = val

      if (old_stat && old_stat !== val) {
        state.messages.splice(0, 1)

        if (state.messages.length) {
          setTimeout(() => { this.dispatch('system/showSnackbar') }, 250)
        }
      }
    },

    setShowLoginForm(state, val) {
      state.show_login_form = val
    },

    addMessage(state, { text, color }) {
      state.messages.push({ text: text, color: color })

      if (!state.snackbar) {
        state.snackbar = true
      }
    }
  },

  actions: {
    showInfo(store, text) {
      store.commit('addMessage', { text: text, color: 'info' })
    },

    showSuccess(store, text) {
      store.commit('addMessage', { text: text, color: 'success' })
    },

    showError(store, text) {
      store.commit('addMessage', { text: text, color: 'error'} )
    },

    showSnackbar(store) {
      store.commit('setSnackbar', true)
    },

    showLoginForm(store) {
      store.commit('setShowLoginForm', true)
    }
  }
}
