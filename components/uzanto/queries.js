// Запрос для получения данных пользователя
export const uzanto_query = `
query ($id: ID!) {
  uzanto(id: $id) {
    unuaNomo {
      enhavo
    }
    duaNomo {
      enhavo
    }
    familinomo {
      enhavo
    }
    sekso
    avataro {
      bildoE {
        url
      }
    }
    kovrilo {
      bildoE {
        url
      }
    }
    statuso {
      enhavo
    }
    statistiko {
      tutaGekamaradoj
      miaGekamarado
      miaGekamaradoPeto
      kandidatoGekamarado
    }
    kontaktaInformo
  }
}`;

// Запрос для получения всех сообщений пользователя
export const enskriboj_query = `
query ($posedantoId: Float!) {
  normo: uzantojMurojEnskriboj(posedantoId: $posedantoId, arkivo: false, forigo: false, orderBy: "-publikiga_dato") {
     edges {
       node{
         uuid
         posedanto {
           uuid
         }
         arkivo
         publikigo
         publikigaDato
         video
         teksto {
           lingvo
           enhavo
         }
         statistiko {
           intereso
           miaIntereso
           tutaTeleportado
         }
         
       }
     }
   }
  	arkivo: uzantojMurojEnskriboj(posedantoId: $posedantoId, arkivo: true, forigo: false, orderBy: "-publikiga_dato") {
     edges {
       node{
         uuid
         posedanto {
           uuid
         }
         arkivo
         publikigo
         publikigaDato
         video
         teksto {
           lingvo
           enhavo
         }
         statistiko {
           intereso
           miaIntereso
           tutaTeleportado
         }
         
       }
     }
   }
  malneto: uzantojMurojEnskriboj(posedantoId: $posedantoId, publikigo: false, forigo: false, orderBy: "-publikiga_dato") {
     edges {
       node{
         uuid
         posedanto {
           uuid
         }
         arkivo
         publikigo
         publikigaDato
         video
         teksto {
           lingvo
           enhavo
         }
         statistiko {
           intereso
           miaIntereso
           tutaTeleportado
         }
         
       }
     }
   }
 }`;

 //мутация, отправляющая запись в архив и обратно, а также удаляющая записи

 export const enskribo_arkivo = `
 mutation($uuid: UUID!, $posedantouuid: UUID!, $arkivo: Boolean, $publikigo: Boolean, $forigo: Boolean) {
  redaktuMuroEnskribo(
    enskriboUuid: $uuid,
    komunumoUuid: $posedantouuid,
    arkivo: $arkivo,
    publikigo: $publikigo
    forigo: $forigo) {
        status
        message
        enskribo{
        __typename
        }
    }
}`;
