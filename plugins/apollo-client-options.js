import {HttpLink} from 'apollo-link-http';

export default function (context) {
  let baseURL = process.env.baseURL;

  if (process.client) {
    baseURL = new URL(process.env.baseURL);
    const currentUrl = new URL(document.location);

    if (currentUrl.hostname !== baseURL.hostname) {
      const port = [80, 443].includes(baseURL.port) ? '' : `:${baseURL.port}`;
      baseURL = `${baseURL.protocol}//${currentUrl.hostname}${port}${baseURL.pathname}`;
    } else {
      baseURL = baseURL.href;
    }
  }

  const WS_PROTOCOL = baseURL.indexOf('https:') === 0 ? 'wss' : 'ws';
  const WS_ENDPOINT = `${WS_PROTOCOL}://${baseURL.replace(/(https:\/\/)|(http:\/\/)/g, '')}ws/`;

  return {
    wsEndpoint: WS_ENDPOINT,
    persisting: false,
    websocketsOnly: true,
  }
}