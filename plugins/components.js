import Vue from 'vue'
import LoginForm from '../components/auth/LoginForm'
import NavBtn from '../components/UI/NavBtn'
import BootomNav from '../components/UI/BootomNav'
import Loader from "../components/UI/loader"

Vue.component('app-login-form', LoginForm)
Vue.component('app-nav-btn', NavBtn)
Vue.component('app-bootom-nav', BootomNav)
Vue.component('app-loader', Loader)
